import mongoose from "mongoose";
import supertest from "supertest";

import { app, server } from "../../main";
import User from "../infrastructure/model/user.model";

const api = supertest(app);

const initialUsers = [
  {
    name: "demo",
    ubicacion: {
      type: "Point",
      coordinates: [-74.006, 40.7128],
    },
  },
  {
    name: "demo2",
    ubicacion: {
      type: "Point",
      coordinates: [-74.006, 40.7128],
    },
  },
];

beforeEach(async () => {
  await User.deleteMany({});

  const user1 = new User(initialUsers[0]);
  await user1.save();

  const user2 = new User(initialUsers[1]);
  await user2.save();
});

test("users return as json", async () => {
  await api
    .get("/users")
    .expect(200)
    .expect("Content-Type", /application\/json/);
});

test("users return data", async () => {
  const response = await api.get("/users");
  expect(response.body).toHaveLength(initialUsers.length);
});

test("the first user is demo", async () => {
  const response = await api.get("/users");

  const names = response.body.map((user: { name: string }) => user.name);

  expect(names).toContain("demo");
});

afterAll(() => {
  mongoose.connection.close();
  server.close();
});
