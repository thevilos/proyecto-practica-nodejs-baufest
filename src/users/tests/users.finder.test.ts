import { UserFinder } from "../application/user-finder";
import { MongoUserRepository } from "../infrastructure/repository/mongo-user-repository";

describe("Users Finder", () => {
  test("UserFinder Return something", async () => {
    const userRepository = new MongoUserRepository();
    const userFinder = new UserFinder(userRepository);

    jest.spyOn(userRepository, "getAll").mockResolvedValue([]);

    const users = await userFinder.getAll();

    expect(users).toEqual([]);
  });
});
