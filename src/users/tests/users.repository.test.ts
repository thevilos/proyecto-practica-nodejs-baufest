import { MongoUserRepository } from "../infrastructure/repository/mongo-user-repository";

describe("Users Repository", () => {
  test("User Return something", async () => {
    const userRepository = new MongoUserRepository();
    jest.spyOn(userRepository, "getAll").mockResolvedValue([]);

    const users = await userRepository.getAll();

    expect(users).toEqual([]);
  });
});
