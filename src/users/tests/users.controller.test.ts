import { UserFinder } from "../application/user-finder";
import { UserController } from "../infrastructure/http/user-controller";
import { MongoUserRepository } from "../infrastructure/repository/mongo-user-repository";

const mockResponse = () => {
  const res: any = {};
  res.status = jest.fn().mockReturnValue(res);
  res.send = jest.fn().mockReturnValue([]);
  return res;
};

describe("Users Controller", () => {
  test("UserController Return something", async () => {
    const userRepository = new MongoUserRepository();
    const userFinder = new UserFinder(userRepository);
    const userController = new UserController(userFinder);

    jest.spyOn(userRepository, "getAll").mockResolvedValue([]);

    const users = await userController.getAll({} as any, mockResponse() as any);

    expect(users).toEqual([]);
  });
});
