import { User } from "../domain/user";
import { UserNotFound } from "../domain/user-not-found";
import { UserRepository } from "../domain/user-repository";

export type Coordinates = {
  lat: number;
  long: number;
};

export class UserFinder {
  constructor(private readonly userRepository: UserRepository) {}

  async getById(id: string): Promise<User> {
    const user = await this.userRepository.getById(id);

    if (!user) {
      throw new UserNotFound(id);
    }

    return user;
  }

  async getAll(): Promise<User[]> {
    const user = await this.userRepository.getAll();
    return user;
  }

  async getAllDisponible(): Promise<User[]> {
    const users = await this.userRepository.getAllDisponible();
    return users;
  }

  async getAllByUbication({ lat, long }: Coordinates): Promise<User[]> {
    const users = await this.userRepository.getAllByUbication({ lat, long });
    return users;
  }

  async create(user: User): Promise<any> {
    const newUser = await this.userRepository.create(user);
    return newUser;
  }
}
