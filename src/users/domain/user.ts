export class User {
  constructor(
    public readonly name: string,
    public readonly disponible: boolean
  ) {}
}

export interface UserResponseDTO {
  ubicacion: {
    type: string;
    coordinates: number[];
  };
  _id?: string;
  name: string;
  disponible: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}
