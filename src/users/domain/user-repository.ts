import { User, UserResponseDTO } from "./user";

export interface UserRepository {
  getById(id: string): Promise<UserResponseDTO | null>;
  getAll(): Promise<UserResponseDTO[]>;
  getAllDisponible(): Promise<UserResponseDTO[]>;
  getAllByUbication({
    lat,
    long,
  }: {
    lat: number;
    long: number;
  }): Promise<UserResponseDTO[]>;
  create(user: User): Promise<UserResponseDTO | null>;
  update(id: string, user: User): Promise<UserResponseDTO | null>;
  delete(id: string): Promise<UserResponseDTO | null>;
}
