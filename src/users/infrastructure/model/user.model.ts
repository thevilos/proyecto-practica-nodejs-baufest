import mongoose, { Document, Schema } from "mongoose";

export interface IUser {
  name: string;
  disponible: boolean;
  distancia: number;
  ubicacion: {
    type: string;
    coordinates: number[];
  };
}

export interface IUserModel extends IUser, Document {}

const UserSchema: Schema = new Schema(
  {
    name: { type: String, required: true },
    disponible: { type: Boolean, default: true },
    distancia: { type: Number, default: 0 },
    ubicacion: {
      type: {
        type: String,
        enum: ["Point"], // Solo permite 'Point' como tipo
        required: true,
      },
      coordinates: {
        type: [Number], // [Longitud, Latitud]
        required: true,
      },
    },
  },
  { timestamps: true }
);

UserSchema.index({ ubicacion: "2dsphere" });

export default mongoose.model<IUserModel>("User", UserSchema);
