import { config } from "../../config";
import { UserFinder } from "../application/user-finder";
import { UserRepository } from "../domain/user-repository";
import { UserController } from "./http/user-controller";
import { MongoUserRepository } from "./repository/mongo-user-repository";

const getUserRepository = (): UserRepository => {
  switch (config.database) {
    case "mongo":
      return new MongoUserRepository();
    default:
      throw new Error("Invalid Database type");
  }
};

const userFinder = new UserFinder(getUserRepository());

export const userController = new UserController(userFinder);
