import express from "express";

import { userController } from "../dependencies";

const userRouter = express.Router();

userRouter.get("/", userController.getAll.bind(userController));
userRouter.get(
  "/ubication",
  userController.getAllByUbication.bind(userController)
);
userRouter.get("/:id", userController.getById.bind(userController));
userRouter.post("/", userController.create.bind(userController));

export { userRouter };
