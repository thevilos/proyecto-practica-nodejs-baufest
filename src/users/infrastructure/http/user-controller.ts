import { Request, Response } from "express";

import { UserFinder } from "../../application/user-finder";
import { UserNotFound } from "../../domain/user-not-found";

export class UserController {
  constructor(private readonly userFinder: UserFinder) {}

  async getById(req: Request, res: Response) {
    const { id } = req.params;

    try {
      const user = await this.userFinder.getById(id);
      return res.status(200).send(user);
    } catch (error) {
      if (error instanceof UserNotFound) {
        return res.status(404).send();
      }

      return res.status(500).send();
    }
  }

  async getAll(req: Request, res: Response) {
    try {
      if (req.query.disponible) {
        const user = await this.userFinder.getAllDisponible();
        return res.status(200).send(user);
      }
      const user = await this.userFinder.getAll();
      return res.status(200).send(user);
    } catch (error) {
      return res.status(500).send();
    }
  }

  async getAllByUbication(req: Request, res: Response) {
    try {
      const user = await this.userFinder.getAllByUbication({
        lat: Number(req.headers.lat as string),
        long: Number(req.headers.long as string),
      });
      return res.status(200).send(user);
    } catch (error) {
      return res.status(500).send(error);
    }
  }

  async create(req: Request, res: Response) {
    try {
      const newUser = await this.userFinder.create(req.body);
      return res.status(200).send(newUser);
    } catch (error) {
      return res.status(500).send();
    }
  }
}
