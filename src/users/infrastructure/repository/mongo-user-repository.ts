import mongoose from "mongoose";

import { User, UserResponseDTO } from "../../domain/user";
import { UserRepository } from "../../domain/user-repository";
import UserMongo from "../model/user.model";

export type Coordinates = {
  lat: number;
  long: number;
};

export class MongoUserRepository implements UserRepository {
  async getById(id: string): Promise<UserResponseDTO | null> {
    return UserMongo.findById(id);
  }

  async getAll(): Promise<UserResponseDTO[]> {
    return UserMongo.find();
  }

  async getAllDisponible(): Promise<UserResponseDTO[]> {
    return UserMongo.find({ disponible: true });
  }

  async getAllByUbication({
    lat,
    long,
  }: Coordinates): Promise<UserResponseDTO[]> {
    const radioEnKilometros = 3;

    return UserMongo.find({
      ubicacion: {
        $near: {
          $geometry: {
            type: "Point",
            coordinates: [long, lat], // En este orden: longitud, latitud
          },
          $maxDistance: radioEnKilometros * 1000, // Convertimos a metros
        },
      },
    });
  }

  async create(user: User): Promise<UserResponseDTO | null> {
    const newUser = new UserMongo({
      _id: new mongoose.Types.ObjectId(),
      name: user.name,
      disponible: user.disponible,
      ubicacion: {
        type: "Point",
        coordinates: [-74.006, 40.7128], // [Longitud, Latitud]
      },
    });

    return newUser.save();
  }

  update(): Promise<UserResponseDTO | null> {
    return null as any;
  }

  delete(): Promise<UserResponseDTO | null> {
    return null as any;
  }
}
