import { UserResponseDTO } from "../../users/domain/user";
import { Pasajero, PasajeroResponseDTO } from "../domain/pasajero";
import { PasajeroRepository } from "../domain/pasajero-repository";

export class PasajeroFinder {
  constructor(private readonly pasajeroRepository: PasajeroRepository) {}

  async getById(id: string): Promise<PasajeroResponseDTO | null> {
    const pasajero = await this.pasajeroRepository.getById(id);

    return pasajero;
  }

  async getAll(): Promise<PasajeroResponseDTO[]> {
    const user = await this.pasajeroRepository.getAll();
    return user;
  }

  async solicitarViaje(pasajero: Pasajero): Promise<UserResponseDTO[] | void> {
    const newPasajero = await this.pasajeroRepository.solicitarViaje(pasajero);
    return newPasajero;
  }
}
