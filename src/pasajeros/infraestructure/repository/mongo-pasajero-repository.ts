import mongoose from "mongoose";

import { UserResponseDTO } from "../../../users/domain/user";
import UserMongo from "../../../users/infrastructure/model/user.model";
import { Pasajero, PasajeroResponseDTO } from "../../domain/pasajero";
import { PasajeroRepository } from "../../domain/pasajero-repository";
import PasajeroMongo from "../model/pasajero.model";

export class MongoPasajeroRepository implements PasajeroRepository {
  async getById(id: string): Promise<PasajeroResponseDTO | null> {
    return PasajeroMongo.findById(id);
  }

  async getAll(): Promise<PasajeroResponseDTO[]> {
    return PasajeroMongo.find();
  }

  async solicitarViaje(pasajero: Pasajero): Promise<UserResponseDTO[] | void> {
    const newPasajero = new PasajeroMongo({
      _id: new mongoose.Types.ObjectId(),
      name: pasajero.name,
      lastName: pasajero.lastName,
      email: pasajero.email,
    });

    await newPasajero.save();

    return UserMongo.find()
      .then((resultados) => {
        // Calcula la distancia a cada lugar desde el punto de partida
        resultados.forEach((user) => {
          const distancia = calcularDistancia(
            pasajero.lat,
            pasajero.long,
            user.ubicacion.coordinates[1],
            user.ubicacion.coordinates[0]
          );
          user.distancia = distancia; // Agrega la distancia al objeto lugar
        });
        // Ordena los lugares por distancia de menor a mayor
        resultados.sort((a, b) => a.distancia - b.distancia);

        // Obtiene los 3 lugares más cercanos
        const lugaresMasCercanos = resultados.slice(0, 3);

        return lugaresMasCercanos;
      })
      .catch((error) => {
        console.error("Error al buscar lugares y calcular distancias:", error);
      });
  }
}

// Función para calcular la distancia entre dos coordenadas geográficas
function calcularDistancia(
  lat1: number,
  lon1: number,
  lat2: number,
  lon2: number
) {
  const radioTierra = 6371; // Radio de la Tierra en kilómetros
  const dLat = (lat2 - lat1) * (Math.PI / 180);
  const dLon = (lon2 - lon1) * (Math.PI / 180);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(lat1 * (Math.PI / 180)) *
      Math.cos(lat2 * (Math.PI / 180)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const distancia = radioTierra * c; // Distancia en kilómetros
  return distancia;
}
