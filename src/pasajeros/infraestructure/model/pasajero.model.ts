import mongoose, { Document, Schema } from "mongoose";

export interface IPasajero {
  name: string;
  lastName: string;
  email: string;
  solicitado: boolean;
}

export interface IPasajeroModel extends IPasajero, Document {}

const PasajeroSchema: Schema = new Schema(
  {
    name: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true },
    solicitado: { type: Boolean, default: true },
  },
  { timestamps: true }
);

export default mongoose.model<IPasajeroModel>("Pasajero", PasajeroSchema);
