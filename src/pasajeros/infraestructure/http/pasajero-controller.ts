import { Request, Response } from "express";

import { PasajeroFinder } from "../../application/pasajero-finder";

export class PasajeroController {
  constructor(private readonly pasajeroFinder: PasajeroFinder) {}

  async getById(req: Request, res: Response) {
    const { id } = req.params;

    try {
      const pasajero = await this.pasajeroFinder.getById(id);
      return res.status(200).send(pasajero);
    } catch (error) {
      return res.status(500).send({ message: "Pasajero no encontrado" });
    }
  }

  async getAll(req: Request, res: Response) {
    try {
      const user = await this.pasajeroFinder.getAll();
      return res.status(200).send(user);
    } catch (error) {
      return res.status(500).send();
    }
  }

  async solicitarViaje(req: Request, res: Response) {
    try {
      const newUser = await this.pasajeroFinder.solicitarViaje(req.body);
      return res.status(200).send(newUser);
    } catch (error) {
      return res.status(500).send();
    }
  }
}
