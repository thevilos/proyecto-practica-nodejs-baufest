import express from "express";

import { pasajeroController } from "../dependencies";

const pasajeroRouter = express.Router();

pasajeroRouter.get("/", pasajeroController.getAll.bind(pasajeroController));
pasajeroRouter.get("/:id", pasajeroController.getById.bind(pasajeroController));
pasajeroRouter.post(
  "/solicitar-viaje",
  pasajeroController.solicitarViaje.bind(pasajeroController)
);

export { pasajeroRouter };
