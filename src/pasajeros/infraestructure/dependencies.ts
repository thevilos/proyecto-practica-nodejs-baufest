import { config } from "../../config";
import { PasajeroFinder } from "../application/pasajero-finder";
import { PasajeroRepository } from "../domain/pasajero-repository";
import { PasajeroController } from "./http/pasajero-controller";
import { MongoPasajeroRepository } from "./repository/mongo-pasajero-repository";

const getPasajeroRepository = (): PasajeroRepository => {
  switch (config.database) {
    case "mongo":
      return new MongoPasajeroRepository();
    default:
      throw new Error("Invalid Database type");
  }
};

const pasajerFinder = new PasajeroFinder(getPasajeroRepository());

export const pasajeroController = new PasajeroController(pasajerFinder);
