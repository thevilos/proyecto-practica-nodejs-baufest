export class Pasajero {
  constructor(
    public readonly name: string,
    public readonly lastName: string,
    public readonly email: string,
    public readonly solicitado: boolean,
    public readonly lat: number,
    public readonly long: number
  ) {}
}

export interface PasajeroResponseDTO {
  _id?: string;
  name: string;
  lastName: string;
  email: string;
  solicitado?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}
