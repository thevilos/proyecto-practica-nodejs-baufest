import { UserResponseDTO } from "../../users/domain/user";
import { Pasajero, PasajeroResponseDTO } from "./pasajero";

export interface PasajeroRepository {
  getById(id: string): Promise<PasajeroResponseDTO | null>;
  getAll(): Promise<PasajeroResponseDTO[]>;
  solicitarViaje(pasajero: Pasajero): Promise<UserResponseDTO[] | void>;
}
