import { Viaje } from "./viaje";

export interface ViajeRepository {
  getAllCompleted(): Promise<any[]>;
  create(viaje: Viaje): Promise<any | null>;
  completeViaje(id: string): Promise<any | null>;
}
