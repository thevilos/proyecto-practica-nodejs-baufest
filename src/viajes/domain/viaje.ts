import { UserResponseDTO } from "../../users/domain/user";

export class Viaje {
  conductorId!: string;
  completed!: boolean;
  name!: string;
}

export interface ViajeResponseDTO {
  _id: string;
  conductorId: UserResponseDTO | string;
  completed: boolean;
  name: string;
}
