import { config } from "../../config";
import { ViajeFinder } from "../application/viaje-finder";
import { ViajeRepository } from "../domain/viaje-repository";
import { ViajeController } from "./http/viaje-controller";
import { MongoViajeRepository } from "./repository/mongo-viaje-repository";

const getViajeRepository = (): ViajeRepository => {
  switch (config.database) {
    case "mongo":
      return new MongoViajeRepository();
    default:
      throw new Error("Invalid Database type");
  }
};

const viajeFinder = new ViajeFinder(getViajeRepository());

export const viajeController = new ViajeController(viajeFinder);
