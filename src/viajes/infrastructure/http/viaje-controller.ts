import { Request, Response } from "express";

import { ViajeFinder } from "../../application/viaje-finder";

export class ViajeController {
  constructor(private readonly viajeFinder: ViajeFinder) {}

  async getAll(req: Request, res: Response) {
    try {
      const viajes = await this.viajeFinder.getAllCompleted();
      return res.status(200).send(viajes);
    } catch (error) {
      return res.status(500).send();
    }
  }

  async create(req: Request, res: Response) {
    try {
      const newViaje = await this.viajeFinder.create(req.body);
      return res.status(200).send(newViaje);
    } catch (error) {
      return res.status(500).send();
    }
  }

  async completeViaje(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const user = await this.viajeFinder.completeViaje(id);
      return res.status(200).send(user);
    } catch (error) {
      return res.status(500).send(error);
    }
  }
}
