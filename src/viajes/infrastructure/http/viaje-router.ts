import express from "express";

import { viajeController } from "../dependencies";

const viajeRouter = express.Router();

viajeRouter.get("/", viajeController.getAll.bind(viajeController));
viajeRouter.post("/", viajeController.create.bind(viajeController));
viajeRouter.put("/:id", viajeController.completeViaje.bind(viajeController));

export { viajeRouter };
