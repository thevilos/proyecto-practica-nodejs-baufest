import mongoose, { Document, Schema } from "mongoose";

export interface IViaje {
  name: string;
  conductorId: string;
  completed: boolean;
}

export interface IViajeModel extends IViaje, Document {}

const ViajeSchema: Schema = new Schema(
  {
    name: { type: String, required: true },
    completed: { type: Boolean, default: false },
    conductorId: { type: Schema.Types.ObjectId, required: true, ref: "User" },
  },
  { timestamps: true }
);

export default mongoose.model<IViajeModel>("Viaje", ViajeSchema);
