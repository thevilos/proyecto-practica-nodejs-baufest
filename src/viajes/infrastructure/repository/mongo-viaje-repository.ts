import mongoose from "mongoose";

import { Viaje, ViajeResponseDTO } from "../../domain/viaje";
import { ViajeRepository } from "../../domain/viaje-repository";
import ViajeMongo from "../model/viaje.model";

export class MongoViajeRepository implements ViajeRepository {
  async getAllCompleted(): Promise<ViajeResponseDTO[]> {
    return ViajeMongo.find({ completed: false }).populate("conductorId");
  }

  async create(viaje: Viaje): Promise<ViajeResponseDTO | null> {
    const newViaje = new ViajeMongo({
      _id: new mongoose.Types.ObjectId(),
      name: viaje.name,
      conductorId: viaje.conductorId,
    });

    return newViaje.save();
  }

  async completeViaje(id: string): Promise<ViajeResponseDTO | null> {
    const viajeCompleted = await ViajeMongo.findByIdAndUpdate(id, {
      completed: true,
    });
    return ViajeMongo.findById(viajeCompleted?._id);
  }
}
