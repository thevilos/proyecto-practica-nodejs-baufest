import { Viaje } from "../domain/viaje";
import { ViajeRepository } from "../domain/viaje-repository";

export class ViajeFinder {
  constructor(private readonly viajeRepository: ViajeRepository) {}

  async getAllCompleted(): Promise<Viaje[]> {
    const viajes = await this.viajeRepository.getAllCompleted();
    return viajes;
  }

  async create(viaje: Viaje): Promise<any> {
    const newViaje = await this.viajeRepository.create(viaje);
    return newViaje;
  }

  async completeViaje(id: string): Promise<any> {
    const newViaje = await this.viajeRepository.completeViaje(id);
    return newViaje;
  }
}
