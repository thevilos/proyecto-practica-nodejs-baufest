import { config as dotEnvConfig } from "dotenv";
dotEnvConfig();

import bodyParser from "body-parser";
import express from "express";
import mongoose from "mongoose";

import { config } from "./config";
import { healthRouter } from "./health/health-router";
import { pasajeroRouter } from "./pasajeros/infraestructure/http/pasajero-router";
import { userRouter } from "./users/infrastructure/http/user-router";
import { viajeRouter } from "./viajes/infrastructure/http/viaje-router";

mongoose
  .connect(config.databaseMongo.url)
  .then(() => {
    console.log("Connected to mongoDB");
  })
  .catch((error) => {
    console.error("Unable to connect: ");
    console.error(error);
  });

// function boostrap() {
export const app = express();

app.use(bodyParser.json());
app.use("/health", healthRouter);
app.use("/users", userRouter);
app.use("/viajes", viajeRouter);
app.use("/pasajeros", pasajeroRouter);

const { port } = config.server;

export const server = app.listen(port, () => {
  console.log(`[APP] - Starting application on port ${port}`);
});

// }

// boostrap();
